<?php
/**
 * 数据库配置
 */

return [
    // 数据库类型
    'database_type' => 'mysql',

    // 数据库名称
    'database_name' => 'movies',

    // 数据库地址
    'server'        => 'localhost',

    // 账号
    'username'      => 'root',

    // 密码
    'password'      => '123456',

    // 端口
    'post'          => 3306,

    // 表前缀
    'prefix'        => '',

    // 是否记录日志
    'logging'       => false,

    // 编码
    'charset'       => 'utf8'
];