<?php

// 数据库配置信息
$db_info = require_once 'config.php';

// 加载公共函数库
require_once 'lib/Common.func.php';

// 采用自动载入类，不用手动去require所需的类文件
spl_autoload_register('autoload');

function autoload($class)
{
    require_once __DIR__ . '/lib/' . $class . '.php';
}