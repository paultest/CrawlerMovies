CREATE TABLE `movies_info` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(255) NOT NULL COMMENT '电影的id',
  `movie_url` varchar(1024) NOT NULL COMMENT '电影链接',
  `movie_name` varchar(1024) NOT NULL COMMENT '电影名称',
  `translation_name` varchar(1024) NOT NULL COMMENT '电影译名',
  `movie_cover` varchar(1024) NOT NULL COMMENT '电影封面',
  `movie_year` int(255) NOT NULL DEFAULT 0 COMMENT '电影年代',
  `movie_country` varchar(1024) NOT NULL COMMENT '电影产地',
  `movie_release_date` varchar(1024) NOT NULL COMMENT '上映日期',
  `movie_length` char(255) NOT NULL COMMENT '电影片长',
  `movie_director` varchar(1024) NOT NULL COMMENT '电影导演',
  `movie_actors` varchar(1024) NOT NULL COMMENT '电影主演',
  `download_url` varchar(1024) NOT NULL COMMENT '下载地址',
  `movie_summary` varchar(2056) NOT NULL COMMENT '电影简介',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`movie_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
