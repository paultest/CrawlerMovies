<?php
/**
 * 公共函数库
 */

/**
 * 抓取网页内容
 * @param $url
 * @return mixed
 */
function CurlGet($url)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_ENCODING, 'gzip');
    $values = curl_exec($curl);

    // 获取响应头大小
    $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);

    // 关闭curl请求
    curl_close($curl);

    // 根据响应头大小去获取头信息内容
    $header = substr($values, 0, $headerSize);

    // 如果响应头中带有Content-Location: http://www.dytt8.net/error.htm?404表示被重定向到404页面了，即该电影不存在，返回false
    if (strpos($header, "Content-Location: http://www.dytt8.net/error.htm?404;") !== false) {
        return false;
    }

    // 转换编码，避免中文乱码
    $values = mb_convert_encoding($values, 'utf-8', 'GBK,UTF-8,ASCII');
    return $values;
}

