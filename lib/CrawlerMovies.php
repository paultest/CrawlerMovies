<?php

/**
 * 爬虫相关操作类
 */
class CrawlerMovies
{
    // 列表页面的变量
    protected $List = [];

    /**
     * 爬取电影天堂的所有电影，并且插入到数据库中
     * @param $list_url
     * @return int 返回插入数据库的电影记录数
     * @throws Exception
     */
    public function run($list_url)
    {
        if (empty($list_url)) {
            throw new Exception('列表的链接为空');
        }

        // 获取列表页面的最大页数以及列表的前缀url
        $this->GetListUrl($list_url);

        // 获取所有的电影页链接
        $url = $this->GetAllMovieUrl();

        // 保存所有的电影链接成csv文件
        $this->SaveCsv($url);

        // 获取所有的电影信息并且插入到数据库
        $num = $this->SaveMovies($url);

        return $num;
    }

    /**
     * 根据url获取单个电影信息
     * @param $url
     * @return array
     * @throws Exception
     */
    public function GetMovieInfo($url)
    {
        if (empty($url)) {
            throw new Exception('链接为空');
        }

        // 抓取网页内容
        $res = CurlGet($url);

        if ($res === false) {
            throw new Exception('该电影不存在');
        }

        // 把中文空格替换成英文空格，如果不替换的话，下面的\s并不能替换中文空格，需要使用中文空格来替换\s
        $res = str_replace('　', ' ', $res);

        // 获取电影详情页面的链接
        $result['movie_url'] = $url;

        // 获取电影的id（在url中可获取）
        $result['movie_id'] = $this->GetMovieId($url);

        // 匹配电影名
        $pattern_name = '/片\s+名\s(.*)\s?<br\s\/>◎年\s+代/';
        preg_match($pattern_name, $res, $movie_name);
        $result['movie_name'] = isset($movie_name[1]) ? trim($movie_name[1]) : '';

        // 匹配译名
        $pattern_translation = '/译\s+名\s(.*)\s?<br\s\/>◎片\s+名/';
        preg_match($pattern_translation, $res, $translation_name);
        $result['translation_name'] = isset($translation_name[1]) ? trim($translation_name[1]) : '';

        // 匹配电影封面
        $pattern_cover = '/<br\s\/><br\s\/><img border="0" src="(.*)" alt=""\s\/>\s<br\s\/><br\s\/>◎译/';
        preg_match($pattern_cover, $res, $movie_cover_res);

        // 如果匹配不到的话则使用旧格式的去找
        if (!isset($movie_cover_res[1])) {
            $pattern_cover = '/alt="" border="0" src="(.*)" \/><br\s\/><span style="COLOR: black"><br\s\/>◎译/';
            preg_match($pattern_cover, $res, $movie_cover_res);
        }
        $result['movie_cover'] = isset($movie_cover_res[1]) ? trim($movie_cover_res[1]) : '';

        // 匹配电影年代
        $pattern_year = '/年\s+代\s(\d+)\s?<br\s\/>◎(产\s+地|国\s+家)/';
        preg_match($pattern_year, $res, $movie_year_res);
        $result['movie_year'] = isset($movie_year_res[1]) ? intval($movie_year_res[1]) : 0;

        // 匹配产地
        $pattern_country = '/(产\s+地|国\s+家)\s(.*)\s?<br\s\/>◎类\s+别/';
        preg_match($pattern_country, $res, $movie_country_res);
        $result['movie_country'] = !empty($movie_country_res) ? trim($movie_country_res[count($movie_country_res) - 1]) : '';

        // 匹配上映日期
        $pattern_release_date = '/上映日期\s(.*)\s<br\s\/>◎IMDb/';
        preg_match($pattern_release_date, $res, $movie_release_date);
        $result['movie_release_date'] = isset($movie_release_date[1]) ? $movie_release_date[1] : '';

        // 匹配片长
        $pattern_length = '/片\s+长\s(.*)\s?<br\s\/>◎导\s+演/';
        preg_match($pattern_length, $res, $movie_length_res);
        $result['movie_length'] = isset($movie_length_res[1]) ? trim($movie_length_res[1]) : '';

        // 匹配导演
        $pattern_director = '/导\s+演\s(.*)\s?<br\s\/>◎主\s+演/';
        preg_match($pattern_director, $res, $movie_director_res);
        $result['movie_director'] = isset($movie_director_res[1]) ? trim($movie_director_res[1]) : '';

        // 匹配主演
        $pattern_actors = '/主\s+演\s(.*)\s?<br\s\/><br\s\/>◎简\s+介/';
        preg_match($pattern_actors, $res, $movie_actors_res);
        $result['movie_actors'] = isset($movie_actors_res[1]) ? trim($movie_actors_res[1]) : '';

        // 匹配简介
        $pattern_summary = '/简\s+介\s<br\s\/><br\s\/>\s+(.*)<br\s\/><br\s\/><img/';
        preg_match($pattern_summary, $res, $movie_summary_res);
        $result['movie_summary'] = isset($movie_summary_res[1]) ? $movie_summary_res[1] : '';

        // 匹配下载地址
        $pattern_download_url = '/<td style="WORD-WRAP: break-word" bgcolor="#fdfddf">(<.*>)?<a href="(.*)">ftp/';
        preg_match($pattern_download_url, $res, $movie_download_url);
        $result['download_url'] = isset($movie_download_url[count($movie_download_url) - 1]) ? trim($movie_download_url[count($movie_download_url) - 1]) : '';

        return $result;
    }

    /**
     * 从列表页面来获取当前列表页所有的电影url
     * @param $list_url string 列表页url
     * @return array
     * @throws Exception
     */
    protected function GetListMovieUrl($list_url)
    {
        // 抓取网页内容
        $res = CurlGet($list_url);

        if ($res === false) {
            throw new Exception('该列表页面不存在');
        }

        // 把中文括号替换成英文括号，如果不替换的话，下面的\s并不能替换中文括号，需要使用中文括号来替换\s
        $res = str_replace('　', ' ', $res);

        // 匹配最后末页列表的url
        $pattern_movie_url = '/<a href="(.*)" class="ulink">/';
        preg_match_all($pattern_movie_url, $res, $movie_url);
        $result = isset($movie_url[1]) ? $movie_url[1] : '';

        if (empty($result)) {
            throw new Exception('获取不到当前列表页的电影URL');
        }

        // 获取域名
        $domain = $this->GetDomain($list_url);

        // 对获取的电影url加上域名，使之为绝对地址
        foreach ($result as $key => $value) {
            $result[$key] = $domain . $value;
        }

        return $result;
    }

    /**
     * 获取列表页面的最大页数以及列表的前缀url
     * @param $list_url string 列表页的url
     * @throws Exception
     */
    protected function GetListUrl($list_url)
    {
        if (!empty($this->List)) {
            return;
        }

        // 抓取网页内容
        $res = CurlGet($list_url);

        if ($res === false) {
            throw new Exception('该列表页面不存在');
        }

        // 把中文括号替换成英文括号，如果不替换的话，下面的\s并不能替换中文括号，需要使用中文括号来替换\s
        $res = str_replace('　', ' ', $res);

        $result = [];

        // 匹配最后末页列表的url
        $pattern_last_page = "/<a href='list_\d+_(\d+).html'>末页<\/a>/";
        preg_match($pattern_last_page, $res, $last_page_url);
        $result['max_page'] = isset($last_page_url[1]) ? intval($last_page_url[1]) : 0;

        // 列表url前缀
        $result['list_url'] = $this->GetListUrlTemplate($list_url);

        if (!empty($result['max_page']) && !empty($result['list_url'])) {
            $this->List = $result;
        }
    }

    /**
     * 获取列表页的链接模板
     * 比如返回： http://www.dytt8.net/html/gndy/dyzz/list_23_%s
     * @param $list_url string 列表页的url
     * @return string
     */
    protected function GetListUrlTemplate($list_url)
    {
        // 获取列表页的链接模板
        $pattern_url_template = "/(.*)_\d+.html/";
        preg_match($pattern_url_template, $list_url, $url_template);
        $result = isset($url_template[1]) ? trim($url_template[1]) . '_%s.html' : '';
        return $result;
    }

    /**
     * 获取域名
     * @param $url string 链接
     * @return string
     */
    protected function GetDomain($url)
    {
        $res = parse_url($url);
        return $res['scheme'] . '://' . $res['host'];
    }

    /**
     * 获取所有列表页下所有的电影页面链接
     * @return array
     * @throws Exception
     */
    protected function GetAllMovieUrl()
    {
        // 所有的电影页面链接数组
        $url = [];

        // 获取所有列表页下所有的电影页面链接
        if (!empty($this->List)) {
            // 列表页最大的页数
            $max_page = isset($this->List['max_page']) ? $this->List['max_page'] : 0;

            // 列表页的链接模板
            $list_url = isset($this->List['list_url']) ? $this->List['list_url'] : '';

            // 获取所有列表页下所有的电影页面链接
            if ($max_page > 0 && !empty($list_url)) {
                for ($i = 1; $i <= $max_page; $i++) {
                    // 获取当前列表页的所有电影url
                    $url = array_merge($this->GetListMovieUrl(sprintf($list_url, $i)), $url);
                }
            }
        }

        // 判断是否能抓到电影链接
        if (empty($url)) {
            throw new Exception('该列表页面抓不到任何的电影链接');
        }

        return $url;
    }

    /**
     * 获取电影的id
     * @param $url
     * @return int
     */
    protected function GetMovieId($url)
    {
        $pattern_movie_id = "%/(\d+).html%";
        preg_match($pattern_movie_id, $url, $movie_id);
        return isset($movie_id[1]) ? intval($movie_id[1]) : 0;
    }

    /**
     * 获取所有电影信息并且存储到数据库
     * @param $url array 所有的电影链接数组
     * @return int 返回插入数据库的数量
     * @throws Exception
     */
    protected function SaveMovies($url)
    {
        // 数据库配置信息
        global $db_info;

        $database = new Medoo($db_info);

        // 开启事务
        $database->pdo->beginTransaction();

        // 插入数据库的数量
        $num = 0;

        try {
            if (!empty($url) && is_array($url)) {
                foreach ($url as $key => $value) {
                    // 根据url获取单个电影信息
                    $data = $this->GetMovieInfo($value);

                    // 保存到数据库
                    if ($this->SaveDataInDb($database, $data)) {
                        ++$num;
                    }
                }
            }

            // 执行事务
            $database->pdo->commit();

            return $num;
        } catch (Exception $e) {
            // 执行回滚
            $database->pdo->rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * 插入单条电影记录到数据库
     * @param $database
     * @param $data
     * @return bool
     * @throws Exception
     */
    protected function SaveDataInDb($database, $data)
    {
        // 判断数据库是否有存在该条记录，有的话则不插入
        if ($this->IsExistMovie($database, $data['movie_id'])) {
            return false;
        }

        // 增加创建时间字段
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

        $database->insert("movies_info", $data);

        // 查看是否有报错
        $error = $database->error();
        if (!empty($error) && isset($error[2])) {
            throw new Exception('数据库报错: ' . $error[2]);
        }

        return true;
    }

    /**
     * 判断数据库是否存在特定的记录
     * @param $database object 数据库实例
     * @param $movie_id int 电影的id
     * @return bool
     */
    protected function IsExistMovie($database, $movie_id)
    {
        $boolen = $database->has('movies_info', ['movie_id' => $movie_id]);

        return $boolen;
    }

    /**
     * 将单条数据导入到Excel
     * @param $data
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Writer_Exception
     */
    protected function WriteExcel($data)
    {
        $PHPExcel = new PHPExcel();
        $PHPExcel_write = new PHPExcel_Writer_Excel5($PHPExcel);

        $string = range('A', 'Z');
        $index = 0;

        // 写入数据
        foreach ($data as $key => $value) {
            // 把key输入到第一行
            $PHPExcel->getActiveSheet()->setCellValue($string[$index] . '1', $key);

            // 把value输入到第二行
            $PHPExcel->getActiveSheet()->setCellValue($string[$index] . '2', $value);

            ++$index;
        }

        // 保存到xls
        $PHPExcel_write->save('data/test.xls');
    }

    /**
     * 保存所有电影链接到csv文件中
     * @param $data
     */
    protected function SaveCsv($data)
    {
        // 打开文件句柄
        $fp = fopen('data/url.csv', 'w');

        // 头部标题
        $csv_header = ['序号', '电影链接'];

        // 处理头部标题
        $header = [];
        foreach ($csv_header as $key => $value) {
            $header[$key] = iconv('utf-8', 'gbk', $value);
        }
        fputcsv($fp, $header);

        // 保存到csv文件中
        $content = [];
        foreach ($data as $k => $v) {
            $content[$k] = [$k, iconv('utf-8', 'gbk', $v)];

            // 保存到csv文件中
            fputcsv($fp, $content[$k]);
        }

        fclose($fp);
    }
}