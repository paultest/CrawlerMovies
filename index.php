<?php
/**
 * 抓取单个电影天堂网页的电影信息
 * 通过post方法把url传过来即可输出
 * 输出格式为json
 * status为0则会出错
 * status为1则成功抓取
 */

require_once 'autoload.php';

try {
    $url = trim($_POST['url']);

    $obj = new CrawlerMovies();

    // 抓取单个url的电影信息
    $data = $obj->GetMovieInfo($url);

    $result = [
        'data'   => $data,
        'status' => 1
    ];
    echo json_encode($result);
    exit();
} catch (Exception $e) {
    echo json_encode(['data' => $e->getMessage(), 'status' => 0]);
    exit();
}
