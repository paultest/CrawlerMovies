<?php
/**
 * 只能在命令行下运行 php main.php [-url http://xxxx.com]
 *
 * 如果后面要加上自己选择的电影天堂的任意一个列表页面链接的话，则在后面加上 -url http://ssss.com 即可
 *
 * 否则的话则使用代码里面的列表页面url
 */

date_default_timezone_set('PRC');

require_once 'autoload.php';

try {
    if (PHP_SAPI != 'cli') {
        throw new Exception('请在命令行下运行!');
    }

    // 自己输入的链接  php main.php -m http://xxx.com
    $input_url = getopt("url::", ["url:"]);

    // 判断有无自己输入的链接，如果有则以自己输入的链接为准，否则的话则固定一个列表页面的url
    if (!empty($input_url) && isset($input_url['url'])) {
        $list_url = $input_url['url'];
    } else {
        // 任意一个列表页面
        $list_url = 'http://www.dytt8.net/html/gndy/dyzz/list_23_25.html';
    }

    // 开始的时间
    $time_start = time();

    // 开始爬电影天堂的所有电影并保存到到数据库中去
    $obj = new CrawlerMovies();
    $num = $obj->run($list_url);

    // 结束的时间
    $time_end = time();

    // 耗时的时间
    $time = intval($time_end - $time_start);

    echo '成功抓取了 '. $num .' 条，耗时 ' . intval($time / 60) . ' 分 ' . $time % 60 . '秒';
} catch (Exception $e) {
    echo $e->getMessage();
}
